#ifndef __UART1_H__
#define __UART1_H__

#define USART_BaudRate_	9600 //115200


/*****************************************************
 * Initialize USART1: enable interrupt on reception
 * of a character
 *****************************************************/
void USART1_Init(void);

// TODO: Add here comments
void USART1_SendBuff(u8 *buff, u16 len);

#endif //__UART1_H__
